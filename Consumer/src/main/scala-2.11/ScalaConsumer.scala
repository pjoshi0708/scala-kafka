import com.google.common.io.Resources
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.consumer.ConsumerRecords
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.errors.WakeupException
import java.util.Arrays
import java.util.Properties
import java.util.Random
import play.api.libs.json.{Reads, Json}
import Messages.FastMessage

import Messages.FastMessageJsonImplicits._


object ScalaConsumer {
  def main(args: Array[String]): Unit = {
    val scalaConsumer = new ScalaConsumer()
    scalaConsumer.run(args)
  }
}

class ScalaConsumer {

  def run(args: Array[String]): Unit = {

    var consumer: KafkaConsumer[String, String] = null

    val props = Resources.getResource("consumer.props").openStream()
    val properties = new Properties()
    properties.load(props)
    if (properties.getProperty("group.id") == null) {
      properties.setProperty("group.id", "group-" + new Random().nextInt(100000))
    }

    consumer = new KafkaConsumer[String, String](properties)
    consumer.subscribe(Arrays.asList("fast-messages"))
    var timeouts = 0

    val t1 = Thread.currentThread()

    Runtime.getRuntime().addShutdownHook(new Thread() {
      override def run() {
        println("Starting exit...")
        consumer.wakeup()
        try {
          t1.join()
        } catch {
          case e: InterruptedException => {
            e.printStackTrace()
          }
        }
      }
    })

    try {
      while (true) {

        println("consumer loop running, wait for messages")
        val records: ConsumerRecords[String, String] = consumer.poll(200)
        val recordCount = records.count()
        if (recordCount == 0) {
          timeouts = timeouts + 1
        } else {

          println(s"Got $recordCount records after $timeouts timeouts\n")
          timeouts = 0
        }

        val iter = records.iterator()
        while (iter.hasNext()) {
          val record: ConsumerRecord[String, String] = iter.next()
          val topic = record.topic()
          topic match {
            case "fast-messages" => {
              readJsonResponse[FastMessage](record, "FastMessage")
            }
          }
        }
      }
    }
    catch {
      case e: WakeupException => {
        println("Wakeup Exception")
      }
    }

    finally {
      consumer.close()
      println("Closed consumer and we are done")
    }
  }

  def readJsonResponse[T](record: ConsumerRecord[String,String], topicDescription : String)(implicit reader: Reads[T]) : Unit = {
    try {
      println(s"$topicDescription >")
      Json.parse(record.value()).asOpt[T].map(rm => println(rm))
    }
    catch {
      case throwable: Throwable =>
        val st = throwable.getStackTrace()
        println(s"readJsonResponse() Got exception : $st")
    }
  }
}