import org.apache.kafka.clients.producer.{KafkaProducer,ProducerRecord}
import java.util.Properties
import com.google.common.io.Resources
import play.api.libs.json.Json
import Messages.FastMessage
import Messages.FastMessageJsonImplicits._


object ScalaProducer {
  def main(args: Array[String]): Unit = {
    val scalaProducer = new ScalaProducer()
    scalaProducer.run(args)

  }
}

class ScalaProducer {

  def run(args: Array[String]) : Unit = {

    var producer: KafkaProducer[String, String] = null

    val props = Resources.getResource("producer.props").openStream()
    val properties = new Properties()
    properties.load(props)
    producer = new KafkaProducer[String, String](properties)
    var jsonText: String = ""
    for (i <- 0 to 1000) {

      jsonText = Json.toJson(FastMessage(s"FastMessage_$i", i)).toString()
      producer.send(new ProducerRecord[String, String]("fast-messages", jsonText))
      producer.flush()
      println("\"Sent msg number : %s", i)
    }
    producer.close()
  }
}
