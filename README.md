# Apache-Kafka-0.9-Simple-Producer_Consumer
Simple Kafka 0.9 producer and consumer with shutdown hook implementation for the Consumer exit on JVM shutdown



Steps:
Follow http://kafka.apache.org/ for reference on setting up Kafka.

1. Download the code and untar it 
tar -xzf kafka_2.11-0.10.0.0.tgz
cd kafka_2.11-0.10.0.0

2. Start the Zookeeper server
bin/zookeeper-server-start.sh config/zookeeper.properties

3. Start the Kafka Server
bin/kafka-server-start.sh config/server.properties

4. Run the producer

5. Run the consumer